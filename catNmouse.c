////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Reid Lum <reidlum@hawaii.edu>
/// @date    21 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
    int DEFAULT_MAX_NUMBER = 2048;
    int theNumberImThinkingof;
    int theMaxValue;
    if (argc == 2) {
        int INPUT_MAX_NUMBER = atoi(argv[1]);
        if (INPUT_MAX_NUMBER >= 1) {
            printf( "OK cat, I'm thinking of a number from 1 to %d.  ", INPUT_MAX_NUMBER);
            theMaxValue = INPUT_MAX_NUMBER;
            theNumberImThinkingof = rand() % theMaxValue;
            //printf("%d   ",theNumberImThinkingof);    This will show theNumberImThinkingof
        }
        else {
            exit(1);
        }    
    }
    
    else {
        printf( "OK cat, I'm thinking of a number from 1 to %d.  ", DEFAULT_MAX_NUMBER);
        theMaxValue = DEFAULT_MAX_NUMBER;
        theNumberImThinkingof = rand() % theMaxValue;
        //printf("%d   ",theNumberImThinkingof); This will show theNumberImThinkingof
    }
  
    int aGuess;
    printf( "Make a guess: " );
    scanf( "%d", &aGuess );
    do {
        if (aGuess > theNumberImThinkingof) {
            printf("No cat... the number I’m thinking of is smaller than %d\n",aGuess);
            printf( "OK cat, I'm thinking of a number from 1 to %d.  ", theMaxValue);
            printf( "Make a guess: " );
            scanf( "%d", &aGuess );
        }
        else if (aGuess < theNumberImThinkingof) {
            printf("No cat... the number I’m thinking of is larger than %d\n",aGuess);
            printf( "OK cat, I'm thinking of a number from 1 to %d.  ", theMaxValue);
            printf( "Make a guess: " );
            scanf( "%d", &aGuess );
        }
    } while (aGuess != theNumberImThinkingof);
    printf("You got me.\n");
    printf("|\\---/|\n");
    printf("| o_o |\n");
    printf(" \\_^_/\n");
   return 0;
 }



